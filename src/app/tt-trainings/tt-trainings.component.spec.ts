import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TtTrainingsComponent } from './tt-trainings.component';

describe('TtTrainingsComponent', () => {
  let component: TtTrainingsComponent;
  let fixture: ComponentFixture<TtTrainingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [TtTrainingsComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TtTrainingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
