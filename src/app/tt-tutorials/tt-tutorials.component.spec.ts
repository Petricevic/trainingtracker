import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TtTutorialsComponent } from './tt-tutorials.component';

describe('TtTutorialsComponent', () => {
  let component: TtTutorialsComponent;
  let fixture: ComponentFixture<TtTutorialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TtTutorialsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TtTutorialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
