import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-app-root',
  templateUrl: './app-root.component.html',
  styleUrls: ['./app-root.component.css']
})
export class AppRootComponent implements OnInit {

  public pageTitle = 'Training Tracker';

  constructor() { }

  ngOnInit() {
  }

}
