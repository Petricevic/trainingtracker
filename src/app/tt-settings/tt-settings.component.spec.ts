import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TtSettingsComponent } from './tt-settings.component';

describe('TtSettingsComponent', () => {
  let component: TtSettingsComponent;
  let fixture: ComponentFixture<TtSettingsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TtSettingsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TtSettingsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
