import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TtAddingExercisesComponent } from './tt-adding-exercises.component';

describe('TtAddingExercisesComponent', () => {
  let component: TtAddingExercisesComponent;
  let fixture: ComponentFixture<TtAddingExercisesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TtAddingExercisesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TtAddingExercisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
