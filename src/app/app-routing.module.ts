import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppRootComponent } from 'src/app/app-root/app-root.component';
import { TtTrainingsComponent } from 'src/app/tt-trainings/tt-trainings.component';
import { TtAddingExercisesComponent } from 'src/app/tt-adding-exercises/tt-adding-exercises.component';
import { TtSettingsComponent } from 'src/app/tt-settings/tt-settings.component';
import { TtGraphsComponent } from 'src/app/tt-graphs/tt-graphs.component';
import { TtTutorialsComponent } from 'src/app/tt-tutorials/tt-tutorials.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    { path: 'application', component: AppRootComponent },
    { path: 'trainings', component: TtTrainingsComponent },
    { path: 'addingExercises', component: TtAddingExercisesComponent },
    { path: 'graphs', component: TtGraphsComponent },
    { path: 'settings', component: TtSettingsComponent },
    { path: 'tutorials', component: TtTutorialsComponent }
  ])],
  exports: [RouterModule]
})
export class AppRoutingModule { }
