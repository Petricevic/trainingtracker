import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TtGraphsComponent } from './tt-graphs.component';

describe('TtGraphsComponent', () => {
  let component: TtGraphsComponent;
  let fixture: ComponentFixture<TtGraphsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TtGraphsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TtGraphsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
