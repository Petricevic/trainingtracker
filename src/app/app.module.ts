import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppRootComponent } from './app-root/app-root.component';
import { TtTrainingsComponent } from './tt-trainings/tt-trainings.component';
import { TtAddingExercisesComponent } from './tt-adding-exercises/tt-adding-exercises.component';
import { TtGraphsComponent } from './tt-graphs/tt-graphs.component';
import { TtSettingsComponent } from './tt-settings/tt-settings.component';
import { TtTutorialsComponent } from './tt-tutorials/tt-tutorials.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatSortModule, MatTableModule, MatDatepickerModule, MatNativeDateModule
} from "@angular/material";



@NgModule({
  declarations: [
    AppComponent,
    AppRootComponent,
    TtTrainingsComponent,
    TtAddingExercisesComponent,
    TtGraphsComponent,
    TtSettingsComponent,
    TtTutorialsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule
  ],
  providers: [MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
